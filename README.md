### Java Spring website

# Repository

## Step 1

Fork this repository
```
https://gitlab.com/marcomybit/mybit-dev-ops-workshop
```

## step 2

Clone your own repository to your local machine with ssh or https:
### ssh
```bash
git clone git@gitlab.com:${gitlab_username}/mybit-dev-ops-workshop.git
```
### https
```bash
git clone https://gitlab.com/${gitlab_username}/mybit-dev-ops-workshop.git
```

## step 3
Change variables:
- ${gitlab_username}
- ${group_name}
- ${gitlab_email}
in the files:
- .gitlab-ci.yml
- .deployment.yml


## step 4
Commit all changes and push it to master branch
```bash
git add -A && git commit -m "Setup variables"
```

```bash
git push
```

Now all configuration is set in this repository

### Optional
If you have mvn and java installed on your PC, you can run it also locally
```bash
mvn spring-boot:run
```

## step 5
Run build for the first time

- Go to CI / CD page on gitlab
- Click on "Rune Pipeline"
- Click again on "Rune Pipeline"

This takes quite a long time, lets go to the next steps

## step 6
Add google token and own password to variables configuration of gitlab

Url: https://gitlab.com/${gitlab_username}/mybit-dev-ops-workshop/-/settings/ci_cd

[img of variable page]

KEY: GOOGLE_KEY value: JSON file provided by teacher
KEY: REGISTRY_PASSWD value: your gitlab password

## step 7
See your build details.

## step 8
Run optional deployment steps "deploy" to deploy it to the cloud.

### Deploy
Deploy docker container with configuration in deployment.yml
```bash
kubectl apply -f deployment.yml
# response
deployment.extensions/${group_name} created
```

## step 9
Run optional deployment steps "expose" to expose container to world wide web.
### Expose
Expose container to world wide web
```bash
kubectl expose deployment ${group_name} --type=LoadBalancer --port=80
# response
service/${group_name} exposed
```

## step 10
Wait a couple of minutes and run last step to get you own uniq public ip

### Expose
Get ip of your load balancer:
```bash
kubectl get services
# response
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)          AGE
kubernetes   ClusterIP      10.59.240.1     <none>          443/TCP          6d21h
user1        LoadBalancer   10.59.241.225   <your-ip>       80:31955/TCP     5m34s
```

Open your ip in the browser to see te result.

## step 10
Now try to change outputted text with a pull request and deploy it again to the cloud to see te result!!

### Help
- Dont forget to fix your unit test
- You don't need to expose it again.

# Variables to set
## Your gitlab username
${gitlab_username}
## Unique groupname to identify your team (use single word without capitals letters or special characters)
${group_name}
## Your gitlab email address
${gitlab_email}
